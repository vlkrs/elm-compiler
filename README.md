# Quick-and-dirty hack to make Elm 0.19.1 build with ghc 9.2.2

This repository is entirely unrelated to the elm project and unmaintained.
The proper repository of the elm compiler is located at https://github.com/elm/compiler

This *should* build with ghc 9.2.2 when invoking ```cabal build --allow-newer```
Feel free to use, but use at your own risk.

------------------------- Original README follows --------------------------

# Elm

A delightful language for reliable webapps.

Check out the [Home Page](http://elm-lang.org/), [Try Online](http://elm-lang.org/try), or [The Official Guide](http://guide.elm-lang.org/)


<br>

## Install

✨ [Install](https://guide.elm-lang.org/install/elm.html) ✨

For multiple versions, previous versions, and uninstallation, see the instructions [here](https://github.com/elm/compiler/blob/master/installers/README.md).

<br>

## Help

If you are stuck, ask around on [the Elm slack channel][slack]. Folks are friendly and happy to help with questions!

[slack]: http://elmlang.herokuapp.com/
